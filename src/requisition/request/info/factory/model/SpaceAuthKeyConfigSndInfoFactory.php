<?php
/**
 * Description :
 * This class allows to define space authentication key configuration sending information factory class.
 * Space authentication key configuration sending information factory uses space authentication key values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * Space authentication key configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     space_auth_key_support_type(optional: got "header", if not found):
 *         "string support type, to set space authentication key options parameters.
 *         Scope of available values: @see ConstSpaceAuthKeyConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     space_auth_key_current_include_config_key(optional):
 *         "string configuration key, to get space authentication key current include option,
 *         used on space authentication key options"
 * ]
 *
 * Space authentication key configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <space_auth_key_current_include_config_key>(optional): true / false
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space_auth_key\requisition\request\info\factory\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\space_auth_key\auth_key\requisition\request\info\library\ConstSpaceAuthKeySndInfo;
use people_sdk\space_auth_key\requisition\request\info\factory\library\ConstSpaceAuthKeyConfigSndInfoFactory;
use people_sdk\space_auth_key\requisition\request\info\factory\exception\ConfigInvalidFormatException;



class SpaceAuthKeyConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstSpaceAuthKeyConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstSpaceAuthKeyConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstSpaceAuthKeyConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured space authentication key current include option, used on space authentication key options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithSpaceAuthKeyCurrentInclude(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstSpaceAuthKeyConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_AUTH_KEY_CURRENT_INCLUDE_CONFIG_KEY]) ?
                $tabConfig[ConstSpaceAuthKeyConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_AUTH_KEY_CURRENT_INCLUDE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstSpaceAuthKeyConfigSndInfoFactory::TAB_CONFIG_KEY_SPACE_AUTH_KEY_SUPPORT_TYPE) ==
                    ConstSpaceAuthKeyConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstSpaceAuthKeySndInfo::HEADER_KEY_CURRENT_INCLUDE : null),
                    ((!$boolOnHeaderRequired) ? ConstSpaceAuthKeySndInfo::URL_ARG_KEY_CURRENT_INCLUDE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithSpaceAuthKeyCurrentInclude();

        // Return result
        return $result;
    }



}