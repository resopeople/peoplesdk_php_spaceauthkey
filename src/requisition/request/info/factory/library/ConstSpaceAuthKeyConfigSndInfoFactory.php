<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space_auth_key\requisition\request\info\factory\library;



class ConstSpaceAuthKeyConfigSndInfoFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_SPACE_AUTH_KEY_SUPPORT_TYPE = 'space_auth_key_support_type';
    const TAB_CONFIG_KEY_SPACE_AUTH_KEY_CURRENT_INCLUDE_CONFIG_KEY = 'space_auth_key_current_include_config_key';

    // Support type configuration
    const CONFIG_SUPPORT_TYPE_URL_ARG = 'url_argument';
    const CONFIG_SUPPORT_TYPE_HEADER = 'header';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the space authentication key configuration sending information factory standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array of support types.
     *
     * @return array
     */
    public static function getTabConfigSupportType()
    {
        // Init var
        $result = array(
            self::CONFIG_SUPPORT_TYPE_URL_ARG,
            self::CONFIG_SUPPORT_TYPE_HEADER
        );

        // Return result
        return $result;
    }



}