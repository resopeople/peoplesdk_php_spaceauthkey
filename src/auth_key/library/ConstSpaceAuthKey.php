<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space_auth_key\auth_key\library;



class ConstSpaceAuthKey
{
    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_KEY = 'strAttrKey';
    const ATTRIBUTE_KEY_KEY_RESET = 'boolAttrKeyReset';
    const ATTRIBUTE_KEY_TYPE = 'strAttrType';
    const ATTRIBUTE_KEY_IS_BLOCKED = 'boolAttrIsBlocked';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_KEY = 'key';
    const ATTRIBUTE_ALIAS_KEY_RESET = 'key-reset';
    const ATTRIBUTE_ALIAS_TYPE = 'type';
    const ATTRIBUTE_ALIAS_IS_BLOCKED = 'is-blocked';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_KEY = 'key';
    const ATTRIBUTE_NAME_SAVE_KEY_RESET = 'key-reset';
    const ATTRIBUTE_NAME_SAVE_TYPE = 'type';
    const ATTRIBUTE_NAME_SAVE_IS_BLOCKED = 'is-blocked';

    // Attribute type configuration
    const ATTRIBUTE_TYPE_USER = 'user';
    const ATTRIBUTE_TYPE_ADMIN = 'admin';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_STANDARD = 'standard';



}