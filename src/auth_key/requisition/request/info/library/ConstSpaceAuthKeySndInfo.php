<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space_auth_key\auth_key\requisition\request\info\library;



class ConstSpaceAuthKeySndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Headers configuration
    const HEADER_KEY_CURRENT_INCLUDE = 'Current-Include';



    // URL arguments configuration
    const URL_ARG_KEY_CURRENT_INCLUDE = 'current-include';



}