<?php
/**
 * This class allows to define space authentication key entity collection class.
 * key => SpaceAuthKeyEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space_auth_key\auth_key\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\space_auth_key\auth_key\model\SpaceAuthKeyEntity;



/**
 * @method null|SpaceAuthKeyEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(SpaceAuthKeyEntity $objEntity) @inheritdoc
 */
class SpaceAuthKeyEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return SpaceAuthKeyEntity::class;
    }



}