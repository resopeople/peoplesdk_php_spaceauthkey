<?php
/**
 * This class allows to define space authentication key entity factory class.
 * Space authentication key entity factory allows to provide new space authentication key entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\space_auth_key\auth_key\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\space_auth_key\auth_key\library\ConstSpaceAuthKey;
use people_sdk\space_auth_key\auth_key\model\SpaceAuthKeyEntity;



/**
 * @method SpaceAuthKeyEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class SpaceAuthKeyEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SpaceAuthKeyEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstSpaceAuthKey::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new SpaceAuthKeyEntity(
            array(),
            $objValidator,
            $objDateTimeFactory
        );

        // Return result
        return $result;
    }



}